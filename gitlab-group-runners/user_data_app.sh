#!/bin/bash
sudo su
curl -L --output /usr/local/bin/gitlab-runner https://gitlab-runner-downloads.s3.amazonaws.com/latest/binaries/gitlab-runner-linux-amd64

# Give it permission to execute
chmod +x /usr/local/bin/gitlab-runner

# Create a GitLab Runner user
useradd --comment 'GitLab Runner' --create-home gitlab-runner --shell /bin/bash

# Install and run as a service
gitlab-runner install --user=gitlab-runner --working-directory=/home/gitlab-runner
gitlab-runner start

gitlab-runner register \
  --non-interactive \
  --url "https://gitlab.com/" \
  --registration-token "GR1348941943VExyBxwhCuBnGP1ps" \
  --executor "docker" \
  --docker-image alpine:latest \
  --description "osp_group_runner" \
  --maintenance-note "Free-form maintainer notes about this runner" \
  --tag-list "osp_group_runner" \
  --run-untagged="true" \
  --locked="false" \
  --access-level="not_protected"

yum install docker -y

systemctl start docker