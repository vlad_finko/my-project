resource "aws_iam_role" "for-ecc2" {
  name = "for-ecc2"
  assume_role_policy  = file("role.json")
  tags = {
    tag-key = "tag-value"
  }
}


resource "aws_iam_role_policy" "policy_one" {
  name = "for-ecc2"
  role =  aws_iam_role.for-ecc2.id
  policy =  file("policy.json")
}

resource "aws_iam_instance_profile" "test_profile" {
  name = "IAMRoleForSSM"
  role = aws_iam_role.for-ecc2.name
}
